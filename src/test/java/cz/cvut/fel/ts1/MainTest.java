package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainTest {
    @Test
    public void factorila() {
        int result = Main.method(2, 2);
        Assertions.assertEquals(4, result);
    }
}

package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KominMaTest {
    @Test
    public void factorialTest()
    {
        KominMa kominMa = new KominMa();
        int result2 = kominMa.factorial(0);
        Assertions.assertEquals(1,result2);
    }
    @Test
    public void factorial_recursive()
    {
        KominMa kominMa = new KominMa();
        int result2 = kominMa.factorialRecursive(5);
        Assertions.assertEquals(120,result2);
    }

}

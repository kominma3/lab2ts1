package cz.cvut.fel.ts1;

public class KominMa {
    public int factorial(int n) {
        if (n < 0) {
            return 0;
        }
        int result = 1;
        for (int i = 0; i < n; i++) {
            result *= (n - i);
        }
        return result;
    }
    public int factorialRecursive(int n){
        if(n <= 1){
            return 1;
        }
        else{
            return n*factorialRecursive(n-1);
        }
    }
}
